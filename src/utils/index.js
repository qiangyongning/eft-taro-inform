import Taro from '@tarojs/taro'

let throttleTimer
// TODO 多处地方同时调用throttle时，如果第一个传的是ahead = true，另外一个传的是ahead = false, 有可能造成两个的method方法都不会被嗲用
export const throttle = ({ method, delay = 300, ahead = false }) => {
  // console.log('已阻止频繁触发..........', ahead)
  if (ahead && !throttleTimer) {
    method()
  }
  clearTimeout(throttleTimer)
  throttleTimer = setTimeout(() => {
    if (!ahead) {
      method()
    }
    clearTimeout(throttleTimer)
    throttleTimer = undefined
  }, delay);
}

export const vibrateShort = () => {
  if (process.env.TARO_ENV != 'h5') {
    Taro.vibrateShort()
  }
}

export const closeWindow = () => {
  const version = Taro.getStorageSync('version')
  const isIPhone = /iphone/g.test(navigator.userAgent.toLowerCase())
  if (process.env.TARO_ENV === 'h5') {
    if (isIPhone && version == '3') {
         window.webkit.messageHandlers.inappbrowserClose.postMessage('')
      return
    }
    window.inappbrowser
      && window.inappbrowser.close
      && window.inappbrowser.close()
  }
}