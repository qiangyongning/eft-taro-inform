import Taro from '@tarojs/taro'
import CryptoJS from 'crypto-js'
import { closeWindow } from "../utils/index";
import md5 from 'md5'

// 处理响应错误
const handleError = (res) => {
  const code = res.statusCode
  switch(code)
  {
    case 401: 
      Taro.showToast({title: '登录已失效', icon: 'loading'})
      closeWindow()
      // if (process.env.TARO_ENV === 'h5' && window.inappbrowser && window.inappbrowser.close) {
      //   setTimeout(() => {
      //     window.inappbrowser.close()
      //   }, 1000)
      // }
      break;
    case 406: 
      Taro.showToast({title: '登录已失效', icon: 'loading'})
      closeWindow()
      // if (process.env.TARO_ENV === 'h5' && window.inappbrowser && window.inappbrowser.close) {
      //   setTimeout(() => {
      //     window.inappbrowser.close()
      //   }, 1000)
      // }
      break;
    default:
      Taro.showToast({title: res.data.msg, icon: 'none'})
  }
}

const setSignHeaders = () => {
  const user = Taro.getStorageSync('user')
  const { userId } = user
  const nonce = Math.random().toString().slice(2, 10);
  const timeStamp = new Date().getTime();
  const signValue = [userId, timeStamp, nonce].join('&');
  const md5SignValue = md5(signValue);
  // if(md5SignValue && md5SignValue.length > 6) {
    const md5SignValueLen = md5SignValue.length;
    const md5SignArr = md5SignValue.split('');
    const firstPart = md5SignArr.slice(0, 6).reverse();
    const lastPart =  md5SignArr.slice(md5SignValueLen - 8, md5SignValueLen).reverse();
    const midPart = lastPart.concat(firstPart).join('');
  // }
  const signature = md5(md5(midPart)).toUpperCase();
  return { timeStamp, nonce, signature }
}

const aesEncrypt = (text) => {
  const key = CryptoJS.enc.Utf8.parse("opxeTuSvZwVoAsEaUqWxlfjuIOtiNxsq");
  const iv = CryptoJS.enc.Utf8.parse("ixohjDEnchErAvfP");
  return CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(text),  key, {iv, mode: CryptoJS.mode.CBC}).toString();
}

// 添加拦截器
const interceptor = (chain) => {
  // 请求信息
  const requestParams = chain.requestParams
  let { method, data, url } = requestParams
  // console.log(`http ${method || 'GET'} --> ${url} data: `, data)
  // 使用chain.proceed调用下一个拦截器或发起请求
  const token = Taro.getStorageSync('token')
  const user = Taro.getStorageSync('user')
  const { tenantId, userId } = user
  if(tenantId && userId) {
    var joiner = url.indexOf('?') !== -1 ? '&' : '?' ;
    var versionStr = '';
    // if(version && htmlVersion) {
    //     versionStr = version + ':' + htmlVersion;
    // }
    requestParams.url = url + joiner + '__u='
    + window.encodeURIComponent(aesEncrypt(tenantId + ':' + userId))
    + '__e=' + window.encodeURIComponent(aesEncrypt(versionStr))
  }
  if (token) {
    requestParams.header.token = token
  }
  // 加签
  requestParams.header = {
    ...requestParams.header, ...setSignHeaders()
  }
  return chain.proceed(requestParams)
    .then(res => {
      // 响应信息
      // console.log(`http <-- ${url} result:`, res)
      return res
    })

}

Taro.addInterceptor(interceptor)

export default {
  request(options, method = 'GET', refresh) {
    if(refresh) {
      Taro.eventCenter.trigger('refreshStart')
    } else {
      Taro.showLoading()
      if (process.env.TARO_ENV === 'weapp' || process.env.TARO_ENV === 'rn') {
        Taro.showNavigationBarLoading()
      }
    }
    // Taro.showLoading()
    return new Promise((resolve, reject) => {
      Taro.request({
          ...options,
          method,
          header: {
            //   'content-type': 'application/x-www-form-urlencoded',
            'content-type': 'application/json',
            ...options.header
          },
        })
        .then((res) => {
          if(refresh) {
            Taro.eventCenter.trigger('refreshEnd')
          } else {
            Taro.hideLoading()
            if (process.env.TARO_ENV === 'weapp' || process.env.TARO_ENV === 'rn') {
              Taro.hideNavigationBarLoading()
            }
          }
          if(res.statusCode != 200) {
            handleError(res)
            reject(res.data)
            return
          }
          if(res.data.success === false) {
            Taro.showToast({title: res.data.msg, icon: 'none'})
          }
          resolve(res.data)
        })
        .catch(error => {
          // Taro.hideLoading()
          Taro.showToast(error)
          if(refresh) {
            Taro.eventCenter.trigger('refreshEnd')
          } else {
            Taro.hideLoading()
            if (process.env.TARO_ENV === 'weapp' || process.env.TARO_ENV === 'rn') {
              Taro.hideNavigationBarLoading()
            }
          }
          reject(error)
        })
    })
  },
  get(options, refresh) {
    return this.request({
      ...options
    }, 'GET', refresh)
  },
  post(options) {
    return this.request({
      ...options,
      data: JSON.stringify(options.data)
    }, 'POST')
  },
  delete(options) {
    return this.request({
      ...options
    }, 'DELETE')
  }
}
