import en_US from './en_US.json'
import zh_CN from './zh_CN.json'
import zh_TW from './zh_TW.json'
import vi_VN from './vi_VN.json'

export const lang = {
    zh_CN, en_US, zh_TW, vi_VN
}