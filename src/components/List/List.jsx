import Taro, { Component } from '@tarojs/taro';
import { View, Text, Button } from '@tarojs/components';
import './List.scss'

class List extends Component {
	constructor() {
		super(...arguments)
		this.state = {}
	}
	render() {
		const listData = this.props.data
		return (
			<View className='List'>
				<View className='list-wrap'>
					<View className='user-wrap'>
						<Image className='user-image' src={listData.userUrl}></Image>
						<View className='user-name'>
							<Text className='nickName'>{listData.nickName}</Text>
							<Text className='deptName'>{listData.deptName}</Text>
						</View>
					</View>
					<View className='comment-content'>
						{listData.commentText}
					</View>
				</View>
			</View>
		);
	}
}
export default List;