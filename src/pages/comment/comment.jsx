import Taro , { Component } from '@tarojs/taro';
import { View, Text , Button, Textarea} from '@tarojs/components';
import './comment.scss'

 class Comment extends Component {

   config = {
       navigationBarTitleText: '评论'
  }

  constructor(){
      super(...arguments)
      this.state = {
          placeholder:'评论不少于60字'
      }
  }

  render() {
    const { data } = this.state
    return (
      <View className='Comment'>
          <View className='comment-wrap'>
              <Textarea placeholder={placeholder} className='wrap'></Textarea>
          </View>
          <View className='comment-btn'>
              <Button>评论</Button>
          </View>
      </View>
    );
  }
}
export default Comment;