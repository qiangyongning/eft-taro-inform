import Taro, { Component } from '@tarojs/taro';
import { View, Text, Button } from '@tarojs/components';
import List from "../../components/List/List";
import './comment_list.scss'

class CommentList extends Component {

	config = {
		navigationBarTitleText: '评论列表'
	}
	constructor() {
		super(...arguments)
		this.state = {
			commentInfo: [
				{
					nickName: 'just_do_it',
					deptName: '大前端部门',
					commentText: '墨刀是一款专业、可靠、简洁、易懂的在线产品原型工具与产品设计团队协作平台,使用墨刀原型设计与协作工具,简单拖拽就能实现页面交互跳转,一键预览还能快速完成原型设计',
					userUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564604680471&di=8c96b845c97cc98913692d1955317d26&imgtype=0&src=http%3A%2F%2Fstatic.tm51.com%2Favatar%2F006%2F07%2F12%2F571559797841_avatar_big.jpg'
				},
				{
					nickName: 'panda格格',
					deptName: '服务端',
					commentText: '墨刀是一款专业、可靠、简洁、易懂的在线产品原型工具与产品设计团队协作平台,使用墨刀原型设计与协作工具,简单拖拽就能实现页面交互跳转,一键预览还能快速完成原型设计',
					userUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564602706480&di=650e1d17c972f5af9c930d0ab0f4d055&imgtype=0&src=http%3A%2F%2Fstatic.tm51.com%2Favatar%2Fdefault%2Fheader%2F10118.jpg'
				},
				{
					nickName: '张三',
					deptName: '测试部门',
					commentText: '墨刀是一款专业、可靠、简洁、易懂的在线产品原型工具与产品设计团队协作平台,使用墨刀原型设计与协作工具,简单拖拽就能实现页面交互跳转,一键预览还能快速完成原型设计',
					userUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1564604794505&di=ea1b2ae3ccdf0383dabbe5079a363e1a&imgtype=0&src=http%3A%2F%2Fstatic.tm51.com%2Favatar%2Fdefault%2Fheader%2F10105.jpg'
				}
			]
		}
	}

	render() {
		return (
			<View className='CommentList'>
				{
					this.state.commentInfo.slice().map(item => <List key={item.id} data={item} />)
				}
			</View>
		);
	}
}
export default CommentList;