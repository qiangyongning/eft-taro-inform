import Taro, { Component } from '@tarojs/taro';
import { View, Text, Button, Image, Input } from '@tarojs/components';
import './login.scss'
class Login extends Component {

    config = {
        navigationBarTitleText: '登录'
    }

    constructor() {
        super(...arguments)
        this.state = {
            userImg: ''
        }
    }
    render() {
        return (
            <View className='Login'>
                <View className='user-info'>
                    <View className='user-Wrap'>
                        <Image className='user-img' src={this.state.userImg}></Image>
                        <Text className='user-name'>强先生</Text>
                    </View>
                    <View className='ellipse'></View>
                </View>
                <View className='login-wrap'>
                    <View className='user'>
                        <Text>强</Text>
                        <Input placeholder='输入账号' />
                    </View>
                    <View className='user pass'>
                        <Text>密</Text>
                        <Input placeholder='输入密码' />
                    </View>
                    <Button className='login-btn'>登 录</Button>
                </View>
                <View className='bottom-text'>
                    <Text>登录后自动绑定微信</Text>
                </View>
            </View>
        );
    }
}
export default Login;