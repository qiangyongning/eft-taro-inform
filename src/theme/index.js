
import theme from './theme.json'
export const createTheme = (tenant) => {
    const {brandColor, navBarText} = theme[tenant] || theme['common']
    return {
        "brandColor": brandColor,
        "secondaryColor": brandColor,
        "navBar": {
            "color": navBarText || "#000000",
            "background": brandColor
        },
        "text": {
            "color": brandColor
        },
        "border": {
            "borderColor":  brandColor
        },
        "button": {
            "background": brandColor,
            "borderColor":  brandColor
        },
        "plainButton": {
            "color": brandColor,
            "borderColor":  brandColor
        },
        "background": {
            "background": brandColor
        },
        "label": {
            "background": `${brandColor || '#01A0FF'}0d`,
            "color": brandColor
        },
        "actionButton": {
            "background": brandColor,
            "boxShadow": `0 1PX 6PX 0 ${brandColor || '#01A0FF'}`
        },
        "score": {
            "color": brandColor
        }
    }
}