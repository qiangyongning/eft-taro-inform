import { observable } from 'mobx'
import Taro from '@tarojs/taro'

import { createTheme } from '../theme'

const themeStore = observable({
    theme: createTheme('dev'),
    tenants: ['tissot', 'longines', 'omega', 'swatch', 'dev'],
    setTheme(tenantId = 'dev') {
        this.theme = createTheme(tenantId || "dev")
    },
    changeTheme(tenantId) {
        this.theme = createTheme(tenantId || "dev")
    }
  })
export default themeStore