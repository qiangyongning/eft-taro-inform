import { observable } from 'mobx'
import Taro from '@tarojs/taro'

import { lang } from '../i18n'

const intlStore = observable({
    local: lang['zh_CN'],
    setLang (language = 'zh_CN') {
        window.local = lang[language]
        this.local = lang[language]
    },
    translate(key) {
        return this.local[key]
    }
})
export default intlStore